# Summary #
This is a small lab project to light up the Consid office christmas tree from a public website using Bluetooth Low Energy beacons, Raspberry Pi, Razberry Z-wave module and a Fibaro wall plug.

# Hardware #
* Raspberry Pi 3
   * Razberry 2 Z-wave module with Z-way controller software.
   * NGINX to serve the web page.
* Three Eddystone URL compatible beacons to broadcast the URL.
* Since beacons can only broadcast URLs served with TLS, we put Cloudflare in front to get `https://` for free.
* Fibaro wall plug (FGWPF-102-ZW5) to turn the christmas tree lighting on or off.


# How it's done #
1. Beacon broadcasts URL and triggers a silent notification on Bluetooth LE compatible Android devices.
2. User clicks the notification and ends up on a public website. Example: https://host.com/christmas
3. On page load, send XHR to Z-way API to read wall plug status. `/status` is mapped to the API call `localhost:8083/ZWaveAPI/Run/devices[5].instances[0].commandClasses[32].data.level.value` in the internal NGINX to restrict public access to the complete Z-way API.
4. If wallplug is on, disable "Turn on" button and vice versa.
5. When user clicks "Turn on" button, send XHR to `/on`, mapped to `localhost:8083/ZWaveAPI/Run/devices[5].instances[0].commandClasses[32].Set(255)`.
6. When user clicks "Turn off", click send XHR to `/off`, mapped to `localhost:8083/ZWaveAPI/Run/devices[5].instances[0].commandClasses[32].Set(0)`.
7. Track turn on/off events in Google Analytics.

# Contact #
samuel.milton@consid.se

# Links #
* Z-way API: http://razberry.z-wave.me/docs/zwayDev.pdf
* Raspberry 3: https://www.m.nu/r-pi/dator-raspberry-pi-3-quad-core-cpu-1gb-ram-wifi-bt
* Razberry 2: https://www.m.nu/styrenheter-z-wave/razberry2-eu-gen5
* Fibaro wall plug: https://www.m.nu/brytare/fibaro-wall-plug
* Beacons: https://www.m.nu/ovrigt-wifi-blatand/radbeacon-dot
