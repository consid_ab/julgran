var ChristmasTree = {

    constants: {
        btnOn: document.getElementById("on"),
        btnOff: document.getElementById("off")
    },

    init: function () {
        ChristmasTree.setupButtonClickEvents(); 
        ChristmasTree.sendXHR("status");
    },

    setupButtonClickEvents: function () {
        var elOn = ChristmasTree.constants.btnOn,
            elOff = ChristmasTree.constants.btnOff;
            
        elOn.addEventListener("click", ChristmasTree.turnOn);
        elOff.addEventListener("click", ChristmasTree.turnOff);
    },

    turnOn: function () {
        gtag('event', 'Button on');
        ChristmasTree.sendXHR("on");
        ChristmasTree.setButtonStatus(true);
    },

    turnOff: function () {
        gtag('event', 'Button off');
        ChristmasTree.sendXHR("off");
        ChristmasTree.setButtonStatus(false);
    },

    sendXHR: function(value, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            var data = request.responseText;
            if (request.readyState == 4 && request.status == 200 && data !== null) {
                if (data === "255") {
                    ChristmasTree.setButtonStatus(true);
                }
                else if (data === "0") {
                    ChristmasTree.setButtonStatus(false);
                }
            }
        }
        request.open('GET', 'https://jul.consid.nu/' + value);
        request.send();
    },

    setButtonStatus: function(isOn) {
        var elOn = ChristmasTree.constants.btnOn,
            elOff = ChristmasTree.constants.btnOff;

        if (isOn) {
            elOn.disabled = true;
            elOff.disabled = false;
        }
        else {
            elOn.disabled = false;
            elOff.disabled = true;
        }
    }
};

document.addEventListener("DOMContentLoaded", function (event) {
    ChristmasTree.init();
});